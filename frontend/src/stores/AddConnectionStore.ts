import { get, post } from '@/core/Request';
import { sortBy } from 'lodash';
import { action, observable } from 'mobx';


export class AddConnectionStore {
  @observable connection;
  @observable selectedDb = '';
  @observable name = '';
  @observable type = '';
  @observable host = '';
  @observable port = '';
  @observable database = '';
  @observable username = '';
  @observable password = '';
  @observable axis: {table: string, colum: string}[] = [];
  @observable currentData: {x: number, y: number}[] = [];


  setAxisTable(i:number, table: string) {
    this.axis[i] = { table, colum: '' };
  }


  setAxisColum(i:number, colum: string) {
    this.axis[i] = { colum, table: this.axis[i].table };
  }


  connect = get<{
    name: string,
    type: string,
    host: string,
    port: string,
    database: string,
    username: string,
    password: string,
  }, any, void>({
    endpoint: '/dbConnection/add',
    success: (data) => {
      this.selectedDb = Object.keys(data)[0];
      this.connection = data;
    },
  });

  getData = get<{
    name: string,
    type: string,
    host: string,
    port: string,
    database: string,
    username: string,
    password: string,
    tableName1: string,
    tableName2: string,
    columName1: string,
    columName2: string,
    join1: string,
    join2: string,
  }, any, void>({
    endpoint: '/dbConnection/getData',
    success: (data) => {
      this.currentData = data;
    },
  });


}

export const addConnectionStore = new AddConnectionStore();
export default addConnectionStore as AddConnectionStore;
