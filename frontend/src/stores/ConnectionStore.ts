import { get, post } from '@/core/Request';
import { sortBy } from 'lodash';
import { action, observable } from 'mobx';


export class ConnectionStore {

  @observable connections: {connection: any, data: {x: number, y: number}[]}[] = [];

  saveAnalysis = get<{
    name: string,
    type: string,
    host: string,
    port: string,
    database: string,
    username: string,
    password: string,
    tableName1: string,
    tableName2: string,
    columName1: string,
    columName2: string,
    join1: string,
    join2: string,
  }, any, void>({
    endpoint: '/dbConnection/saveAnalysis',
  });


  getConnectionData = get<{
  }, any, void>({
    endpoint: '/dbConnection/getConnectionData',
    success: (data) => {
      this.connections = data;
    },
  });




}

export const connectionStore = new ConnectionStore();
export default connectionStore as ConnectionStore;
