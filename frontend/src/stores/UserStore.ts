import { get, post } from '@/core/Request';
import { each, findIndex, sortBy } from 'lodash';
import { action, computed, observable } from 'mobx';

export class UserStore {
  @observable firstName: string;
  @observable lastName: string;
  @observable email: string;
  @observable password: string;
  @observable sessionKey: string;
  @observable _sessionId: string;

  @computed get sessionId() {
    return this._sessionId || window.sessionStorage.getItem('session_id');
  }

  @computed get authenticated(): boolean {
    //return !!this.sessionId;
    return true;
  }

  authenticat(sessionId) {
    window.sessionStorage.setItem('session_id', sessionId);
    this._sessionId = sessionId;
  }

  register = post<{
    firstName: string,
    lastName: string,
    email: string,
    password: string,
  },  {error?: string},  {error?: string}>({
    endpoint: '/user',
    success: (data) => {
      return data;
    },
  });


  login = get< {
    email: string,
    password: string,
  }, {sessionKey?: string, error?: string}, {sessionKey?: string, error?: string}>({
    endpoint: '/login',
    success: (data) => {
      this.authenticat(data.sessionKey);
      return data;
    },
  });

}

export const userStore = new UserStore();
export default userStore as UserStore;
