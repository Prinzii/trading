
import Card from '@material-ui/core/Card';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import * as theme from './theme.scss';

interface ISnackbarbarProps {
  open: boolean;
  onClose?: () => void;
  type?: 'Info' | 'Error';
}


export default class MySnackbar extends React.Component<ISnackbarbarProps> {

  getColor() {
    if (this.props.type === 'Error') {
      return '#f44336';
    }
    return '#4caf50';
  }

  public render(): JSX.Element {
    return (
      <Snackbar
        open={this.props.open}
        autoHideDuration={6000}
        onClose={this.props.onClose}>
        <div style={{
          color: '#ffffff',
          fontWeight: 500,
          borderRadius: '4px',
          backgroundColor: this.getColor(),
          padding: '16px',
          boxShadow: '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12)',
        }}>
          {this.props.children}
        </div>
      </Snackbar >
    );
  }
}
