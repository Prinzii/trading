export const pageWrapper: string;
export const card: string;
export const fadeIn: string;
export const largeCard: string;
export const button: string;
export const form: string;
