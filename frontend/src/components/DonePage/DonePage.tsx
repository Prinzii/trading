
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import * as classNames from 'classnames';
import * as React from 'react';
import * as theme from './theme.scss';

interface IDonePageProps {
  onClick?: () => void;
  buttonText: string;
  smallHeader: string;
  largeHeader: string;
}

export default class DonePage extends React.Component<IDonePageProps> {

  public render(): JSX.Element {
    return (
      <div className={theme.pageWrapper}>
        <Card className={classNames(theme.card, theme.largeCard)}>
          <Typography variant="body2" color="textSecondary" component="p">
            {this.props.smallHeader}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
            {this.props.largeHeader}
          </Typography>
          <Button
            variant="contained"
            color="primary"
            className={theme.button}
            onClick={this.props.onClick}
          >
            {this.props.buttonText}
          </Button>
        </Card>
      </div >
    );
  }
}
