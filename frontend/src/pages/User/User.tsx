import { AddConnectionStore, ConnectionStore } from '@/stores';
import * as globalTheme from '@/styles/globalTheme.scss';
import AppBar from '@material-ui/core/AppBar';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import * as classNames from 'classnames';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import * as React from 'react';
import * as GridLayout from 'react-grid-layout';
import AddData from './AddData';
import DataView from './DataView';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import UserIcon from '@material-ui/icons/Person';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';

import * as theme from './theme.scss';

import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';

interface ILoginProps {
  history: any;
  location: any;
  connectionStore?: ConnectionStore;
  addConnectionStore?: AddConnectionStore;
}

@inject('connectionStore', 'addConnectionStore') @observer
export default class User extends React.Component<ILoginProps> {
@observable showDrawer: boolean = false;

  public render(): JSX.Element {
    return (
      <div className={theme.pageWrapper}>
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu" onClick={()=>{this.showDrawer = true}}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              User
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer style={{minWidth:"240px"}} open={this.showDrawer} onBackdropClick={()=>{this.showDrawer=false}}>
          <List>
          <ListItem button
          onClick={ ()=> {
            window.location.href = "data"
          }}
          >
             <ListItemIcon><WorkIcon/></ListItemIcon>
             <ListItemText primary={"Dashboard"} />
           </ListItem>
           <ListItem button
           onClick={ ()=> {
             window.location.href = "user"
           }}
           >
             <ListItemIcon><UserIcon/></ListItemIcon>
             <ListItemText primary={"User"} />
           </ListItem>
         </List>
        </Drawer>
        <List>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <UserIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Alexander Prinz" secondary="alexander.prinz@hotmail.com" />
          </ListItem>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <UserIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Test Test" secondary="test.test@test.com" />
          </ListItem>
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <UserIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Test Huber" secondary="huber@test.at" />
          </ListItem>
        </List>
      </div >
    );
  }
}
