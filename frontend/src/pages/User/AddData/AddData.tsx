
import { AddConnectionStore, ConnectionStore } from '@/stores';
import Button from '@material-ui/core/Button';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import * as theme from './theme.scss';
import ConnectDB, { IConnectDBState } from './Views/ConnectDB';
import SelectData from './Views/SelectData';
import SelectDiagram from './Views/SelectDiagram';

import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

interface IAddDataProps {
  connectionStore?: ConnectionStore;
  addConnectionStore?: AddConnectionStore;
  onFinish: () => void;
}

@inject('connectionStore', 'addConnectionStore') @observer
export default class AddData extends React.Component<IAddDataProps> {
  steps = ['Connect to Database', 'Select a Plot', 'Select the Data'];
  @observable activeStep: number = 0;
  connectionValue: IConnectDBState;

  handleStep = (step) => {
    if (step < this.steps.length && step >= 0) {
      this.activeStep = step;
    }
  }

  getView = () => {
    switch (this.activeStep) {
      case 0:
        return <ConnectDB
          onValueChange={
            (data) => { this.connectionValue = data; }
          }
        />;
      case 1:
        return <SelectData
          connection={this.props.addConnectionStore.connection}
        />;
      case 2:
        return <SelectDiagram
          connection={this.props.addConnectionStore.connection}
        />;
      default:
        break;
    }
  }


  public render(): JSX.Element {
    return (
      <>
        <DialogTitle>
          <Stepper nonLinear activeStep={this.activeStep}>
            {this.steps.map((label, index) => (
              <Step completed={true} key={label}>
                <StepButton onClick={() => this.handleStep(index)} completed={false}>
                  {label}
                </StepButton>
              </Step>
            ))}
          </Stepper>
        </DialogTitle>
        <DialogContent dividers>
          {this.getView()}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              if (this.activeStep === 0) {
                this.props.addConnectionStore.connect({
                  name: this.props.addConnectionStore.name,
                  type: this.props.addConnectionStore.type,
                  host: this.props.addConnectionStore.host,
                  port: this.props.addConnectionStore.port,
                  database: this.props.addConnectionStore.database,
                  username: this.props.addConnectionStore.username,
                  password: this.props.addConnectionStore.password,
                });
              }
              if (this.activeStep === 2) {
                this.props.connectionStore.saveAnalysis({
                  name: this.props.addConnectionStore.name,
                  type: this.props.addConnectionStore.type,
                  host: this.props.addConnectionStore.host,
                  port: this.props.addConnectionStore.port,
                  database: this.props.addConnectionStore.database,
                  username: this.props.addConnectionStore.username,
                  password: this.props.addConnectionStore.password,
                  tableName1: this.props.addConnectionStore.axis[0].table,
                  tableName2: this.props.addConnectionStore.axis[1].table,
                  columName1: this.props.addConnectionStore.axis[0].colum,
                  columName2: this.props.addConnectionStore.axis[1].colum,
                  join1: 'id',
                  join2: 'id',
                });
                this.props.onFinish();
                return;
              }
              this.handleStep(this.activeStep + 1);
            }}
            color="primary"
          >
            {this.activeStep === 2 ? 'Add' : 'Next'}
          </Button>
        </DialogActions>
      </ >
    );
  }
}
