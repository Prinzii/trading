
import { AddConnectionStore, ConnectionStore } from '@/stores';
import TextField from '@material-ui/core/TextField';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import * as theme from './theme.scss';

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import { CartesianGrid, Line, Scatter, ScatterChart, XAxis, YAxis } from 'recharts';

interface ISelectDataProps {
  connectionStore?: ConnectionStore;
  connection: any;
  addConnectionStore?: AddConnectionStore;
}

interface TabPanelProps {
  children?: React.ReactNode;
  getTablesForDropdown: () => JSX.Element[];
  getTableHeader: () => JSX.Element[];
  getTableBody: () => JSX.Element[];
  connectionStore: ConnectionStore;
  index: any;
  value: any;
}


@inject('connectionStore', 'addConnectionStore') @observer
export default class SelectData extends React.Component<ISelectDataProps> {


  componentWillMount() {
    this.props.addConnectionStore.getData({
      name: this.props.addConnectionStore.name,
      type: this.props.addConnectionStore.type,
      host: this.props.addConnectionStore.host,
      port: this.props.addConnectionStore.port,
      database: this.props.addConnectionStore.database,
      username: this.props.addConnectionStore.username,
      password: this.props.addConnectionStore.password,
      tableName1: this.props.addConnectionStore.axis[0].table,
      tableName2: this.props.addConnectionStore.axis[1].table,
      columName1: this.props.addConnectionStore.axis[0].colum,
      columName2: this.props.addConnectionStore.axis[1].colum,
      join1: 'id',
      join2: 'id',
    });
  }

  public render(): JSX.Element {

    return (
      <div>
        <ScatterChart
          width={500}
          height={400}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="x" />
          <YAxis type="number" dataKey="y" />
          <Scatter
            name="A school"
            data={this.props.addConnectionStore.currentData}
            fill="#8884d8"
            line
          />
        </ScatterChart>
      </div>
    );
  }

}
