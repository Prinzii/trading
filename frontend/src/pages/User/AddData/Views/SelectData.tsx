
import { AddConnectionStore, ConnectionStore } from '@/stores';
import TextField from '@material-ui/core/TextField';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import * as theme from './theme.scss';

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';

interface ISelectDataProps {
  connectionStore?: ConnectionStore;
  connection: any;
  addConnectionStore?: AddConnectionStore;
}

interface TabPanelProps {
  children?: React.ReactNode;
  getTablesForDropdown: () => JSX.Element[];
  getTableHeader: () => JSX.Element[];
  getTableBody: () => JSX.Element[];
  connectionStore: ConnectionStore;
  index: any;
  value: any;
}


@inject('connectionStore', 'addConnectionStore') @observer
export default class SelectData extends React.Component<ISelectDataProps> {
  steps = ['Connect to Database', 'Select a Plot', 'Select the Data'];

  createData(name: string, calories: number, fat: number, carbs: number, protein: number) {
    return { name, calories, fat, carbs, protein };
  }

  @observable activeStep: number = 0;
  @observable hoverColum: number = 0;
  @observable selectedCell: number = 1;
  @observable currentAxis: number = 0;

  handleStep = (step) => {
    this.activeStep = step;
  }

  getTablesForDropdown = () => {
    return Object.keys(this.props.addConnectionStore.connection || {}).map((table) => {
      return (
        <MenuItem value={table}>{table}</MenuItem>
      );
    });
  }

  getColumName = (table: string, index: number) => {
    const currentTable = this.props.addConnectionStore.connection[table];
    if (!currentTable) {
      return '';
    }
    const firstRow = currentTable[0];
    if (firstRow) {
      return Object.keys(firstRow)[index];
    }
  }


  getTableHeader = () => {
    const crrentTableKey = this.props.addConnectionStore.selectedDb;
    if (!this.props.addConnectionStore.connection) {
      return [];
    }
    const currentTable = this.props.addConnectionStore.connection[crrentTableKey];
    if (!currentTable) {
      return [];
    }
    const firstRow = currentTable[0];
    return Object.keys(firstRow || {}).map((colum, i) => {
      let style;
      if (i === this.hoverColum) {
        style = { background: '#e8eaf6' };
      }
      let selected = false;
      if (this.props.addConnectionStore.axis[this.currentAxis]) {
        selected = (
          this.props.addConnectionStore.selectedDb === this.props.addConnectionStore.axis[this.currentAxis].table &&
          colum === this.props.addConnectionStore.axis[this.currentAxis].colum
        );
      }
      return (
        <TableCell
          className={
            classNames(theme.cell, {
              [theme.selectedCell]: selected,
            })
          }
          onClick={() => {
            this.props.addConnectionStore.setAxisTable(
              this.currentAxis, this.props.addConnectionStore.selectedDb,
            );
            this.props.addConnectionStore.setAxisColum(
              this.currentAxis, colum,
            );
            this.selectedCell = i;
          }}
          onMouseEnter={() => { this.hoverColum = i; }}
          style={style}
        >
          {colum}
        </TableCell>
      );
    });
  }

  getTableBody = () => {
    const crrentTableKey = this.props.addConnectionStore.selectedDb;
    if (!this.props.addConnectionStore.connection) {
      return [];
    }
    const currentTable = this.props.addConnectionStore.connection[crrentTableKey];
    const keys = Object.keys(currentTable[0] || {});
    const returnArray = [];

    for (let i = 0; i < currentTable.length; i++) {
      const row = currentTable[i];

      returnArray.push(
        <TableRow>
          {
            keys.map((cell, j) => {
              let selected = false;
              const columName = this.getColumName(this.props.addConnectionStore.selectedDb, j);
              if (this.props.addConnectionStore.axis[this.currentAxis]) {
                selected = (
                  this.props.addConnectionStore.selectedDb === this.props.addConnectionStore.axis[this.currentAxis].table &&
                  columName === this.props.addConnectionStore.axis[this.currentAxis].colum
                );
              }
              let style;
              if (j === this.hoverColum) {
                style = { background: '#e8eaf6' };
              }
              return (
                <TableCell
                  className={
                    classNames(theme.cell, {
                      [theme.selectedCell]: selected,
                    })
                  }
                  onClick={() => {
                    this.props.addConnectionStore.setAxisTable(
                      this.currentAxis, this.props.addConnectionStore.selectedDb,
                    );
                    this.props.addConnectionStore.setAxisColum(
                      this.currentAxis, columName,
                    );
                    this.selectedCell = j;
                  }}
                  onMouseEnter={() => { this.hoverColum = j; }}
                  style={style}>{row[cell]}
                </TableCell>
              );
            })
          }
        </TableRow>);
    }
    return returnArray;
  }

  getTabPanel(index, value) {

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
      >
        {value === index && (
          <div>
            <FormControl style={{ margin: '12px' }} variant="outlined" >
              <InputLabel id="demo-simple-select-outlined-label">Table</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={this.props.addConnectionStore.selectedDb}
                onChange={(event) => {
                  this.props.addConnectionStore.selectedDb = event.target.value as any as string;
                }}
              >
                {this.getTablesForDropdown()}
              </Select>
            </FormControl>
            <Table
              onMouseLeave={() => { this.hoverColum = -1; }}
              aria-label="simple table"
            >
              <TableHead>
                <TableRow>
                  {this.getTableHeader()}
                </TableRow>
              </TableHead>
              <TableBody>
                {this.getTableBody()}
              </TableBody>
            </Table>
          </div>
        )}
      </div>
    );
  }


  public render(): JSX.Element {

    return (
      <div style={{ margin: '-12px -24px' }}>
        <div>X: {JSON.stringify(this.props.addConnectionStore.axis[0])}</div>
        <div>Y: {JSON.stringify(this.props.addConnectionStore.axis[1])}</div>
        <AppBar position="static">
          <Tabs
            indicatorColor="primary"
            textColor="primary"
            style={{ background: 'white' }}
            value={this.currentAxis}
            onChange={(_e, i) => { this.currentAxis = i; }}
            aria-label="simple tabs example"
          >
            <Tab label="X-Data" />
            <Tab label="Y-Data" />
          </Tabs>
        </AppBar>
        {this.getTabPanel(0, this.currentAxis)}
        {this.getTabPanel(1, this.currentAxis)}
      </div>
    );
  }

}
