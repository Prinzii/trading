
import { AddConnectionStore, ConnectionStore } from '@/stores';
import TextField from '@material-ui/core/TextField';
import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import * as theme from './theme.scss';



interface IConnectDBProps {
  connectionStore?: ConnectionStore;
  addConnectionStore?: AddConnectionStore;
  onValueChange: (data: IConnectDBState) => void;
}

export interface IConnectDBState {
  name: string;
  type: string;
  host: string;
  port: string;
  database: string;
  username: string;
  password: string;
}

@inject('connectionStore', 'addConnectionStore') @observer
export default class ConnectDB extends React.Component<IConnectDBProps, IConnectDBState> {
  steps = ['Connect to Database', 'Select a Plot', 'Select the Data'];
  @observable activeStep: number = 0;

  state = {
    name: '',
    type: '',
    host: '',
    port: '',
    database: '',
    username: '',
    password: '',
  };

  handleStep = (step) => {
    this.activeStep = step;
  }

  public render(): JSX.Element {
    return (
      <div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.name}
            onChange={(e) => {
              this.props.addConnectionStore.name = e.target.value;
            }}
            className={theme.input}
            label="Name"
            variant="outlined" />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.type}
            onChange={(e) => {
              this.props.addConnectionStore.type = e.target.value;
            }}
            className={theme.input}
            label="Type"
            variant="outlined" />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.host}
            onChange={(e) => {
              this.props.addConnectionStore.host = e.target.value;
            }}
            className={theme.input}
            label="Host"
            variant="outlined"
          />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.port}
            onChange={(e) => {
              this.props.addConnectionStore.port = e.target.value;
            }}
            className={theme.input}
            label="Port"
            variant="outlined"
          />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.database}
            onChange={(e) => {
              this.props.addConnectionStore.database = e.target.value;
            }}
            className={theme.input}
            label="Database"
            variant="outlined" />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.username}
            onChange={(e) => {
              this.props.addConnectionStore.username = e.target.value;
            }}
            className={theme.input}
            label="Username"
            variant="outlined"
          />
        </div>
        <div className={theme.wrapper}>
          <TextField
            value={this.props.addConnectionStore.password}
            onChange={(e) => {
              this.props.addConnectionStore.password = e.target.value;
            }}
            className={theme.input}
            label="Password"
            variant="outlined" />
        </div>
      </div>
    );
  }
}
