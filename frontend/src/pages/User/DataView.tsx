
import { AddConnectionStore, ConnectionStore } from '@/stores';
import * as globalTheme from '@/styles/globalTheme.scss';
import AppBar from '@material-ui/core/AppBar';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import Fab from '@material-ui/core/Fab';

import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import * as classNames from 'classnames';
import * as React from 'react';
import { CartesianGrid, Line, Scatter, ScatterChart, XAxis, YAxis } from 'recharts';
import AddData from './AddData';
import * as theme from './theme.scss';


import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';

interface ILoginProps {
  connectionStore?: ConnectionStore;
  addConnectionStore?: AddConnectionStore;
  data: { x: number, y: number }[];
  name: string;
}

@inject('connectionStore', 'addConnectionStore') @observer
export default class DataView extends React.Component<ILoginProps> {


  public render(): JSX.Element {

    return (
      <div className={theme.card}>
        <Typography variant="h6" style={{ margin: '6px' }}>
          {this.props.name}
        </Typography>
        <ScatterChart
          width={500}
          height={400}
          margin={{
            top: 20, right: 20, bottom: 20, left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="x" />
          <YAxis type="number" dataKey="y" />
          <Scatter
            name="A school"
            data={this.props.data}
            fill="#8884d8"
            line
          />
        </ScatterChart>
      </div>
    );
  }
}
