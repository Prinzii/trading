// import * as theme from '@/styles/main.scss';
import { ENV } from '@/constants';
import Login from '@/pages/Login';
import User from '@/pages/User';
import Registration from '@/pages/Registration';
import { addConnectionStore, connectionStore, lottoStore, userStore } from '@/stores';
import { inject, observer, Provider } from 'mobx-react';
import DevTools from 'mobx-react-devtools';
import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';




class App extends React.Component {

  private showDevTools(): JSX.Element {
    if (ENV === 'development' && false) {

      return (
        <div>
          <DevTools />
        </div>
      );

    }

    return null;
  }


  getBookingPage(props) {
    if (userStore.authenticated) {
      return <Login {...props} />;
    }
    return (<Registration {...props} />);
  }

  getUserPage(props) {
    if (userStore.authenticated) {
      return <User {...props} />;
    }
    return (<Registration {...props} />);
  }

  render() {
    return (
      <Provider
        lottoStore={lottoStore}
        userStore={userStore}
        connectionStore={connectionStore}
        addConnectionStore={addConnectionStore}
      >
        <BrowserRouter>
          <div>
            {this.showDevTools()}
            <Switch>
              <Route exact path={'/data'} render={(props) => this.getBookingPage(props)} />
              <Route exact path={'/user'} render={(props) => this.getUserPage(props)} />
              <Route exact path={'/'} render={(props) => this.getBookingPage(props)} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
