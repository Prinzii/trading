
import DonePage from '@/components/DonePage';
import Snackbar from '@/components/Snackbar';
import { UserStore } from '@/stores';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as classNames from 'classnames';
import * as React from 'react';
import * as theme from './theme.scss';


import { observable, toJS } from 'mobx';
import { inject, observer } from 'mobx-react';

interface ILoginProps {
  userStore?: UserStore;
  history: any;
  location: any;
}


@inject('userStore') @observer
export default class BookingCarPage extends React.Component<ILoginProps> {

  @observable activeTab = 0;
  @observable registrationView = false;
  @observable waiting = false;
  @observable open = false;
  @observable message = '';
  @observable type: 'Info' | 'Error' = 'Info';

  getRegistrationHash(): string {
    const hash = this.props.location.pathname.match(/\/registration\/([0-9a-z]+)/);
    if (hash) {
      return hash[1];
    }
    return null;
  }

  isLoginRout(): boolean {
    return !!this.props.location.pathname.match(/\/login/);
  }


  handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    this.activeTab = newValue;
  }

  setFirstName(e) {
    this.props.userStore.firstName = e.target.value;
  }

  setLastName(e) {
    this.props.userStore.lastName = e.target.value;
  }

  setEmail(e) {
    this.props.userStore.email = e.target.value;
  }

  setPassword(e) {
    this.props.userStore.password = e.target.value;
  }

  getRegistrationGui() {
    if (this.waiting) {
      return null;
    }
    return (
      <>
        <Typography className={theme.header} gutterBottom variant="h5" component="h2">
          Register
        </Typography>
        <TextField
          onChange={(e) => this.setFirstName(e)}
          value={this.props.userStore.firstName}
          className={theme.form}
          id="outlined-basic"
          label="First Name"
          variant="outlined"
        />
        <TextField
          onChange={(e) => this.setLastName(e)}
          value={this.props.userStore.lastName}
          className={theme.form}
          id="outlined-basic"
          label="Last Name"
          variant="outlined"
        />
        <TextField
          onChange={(e) => this.setEmail(e)}
          value={this.props.userStore.email}
          className={theme.form}
          id="outlined-basic"
          label="E-mail"
          variant="outlined"
        />
        <TextField
          onChange={(e) => this.setPassword(e)}
          value={this.props.userStore.password}
          className={theme.form}
          id="outlined-basic"
          type="password"
          label="Password"
          variant="outlined"
        />
        <Button
          variant="contained"
          color="primary"
          className={theme.button}
          onClick={() => {
            this.props.userStore.register({
              firstName: this.props.userStore.firstName,
              lastName: this.props.userStore.lastName,
              email: this.props.userStore.email,
              password: this.props.userStore.password,
            }).then((data) => {
              if (data && data.error) {
                this.message = data.error;
                this.open = true;
                this.type = 'Error';
                return;
              }
              this.open = true;
              this.message = 'Email for authentication send';
              this.type = 'Info';
              this.activeTab = 0;
            });
          }}
        >
          Register
        </Button>
      </>
    );
  }

  getLoginnGui() {
    return (
      <>
        <Typography className={theme.header} gutterBottom variant="h5" component="h2">
          Login
        </Typography>
        <TextField
          onChange={(e) => this.setEmail(e)}
          value={this.props.userStore.email}
          className={theme.form}
          id="outlined-basic"
          label="E-mail"
          variant="outlined"
        />
        <TextField
          onChange={(e) => this.setPassword(e)}
          value={this.props.userStore.password}
          className={theme.form}
          id="outlined-basic"
          type="password"
          label="Password"
          variant="outlined"
        />
        <Button
          variant="contained"
          color="primary"
          className={theme.button}
          onClick={() => {
            this.props.userStore.login({
              email: this.props.userStore.email,
              password: this.props.userStore.password,
            }).then((data) => {
              if (data && data.error) {
                this.message = data.error;
                this.open = true;
                this.type = 'Error';
              }
              if (data && data.sessionKey) {
                if (this.isLoginRout()) {
                  this.props.history.push('/');
                } else {
                  location.reload();
                }
              }
            });
          }}
        >
          Login
        </Button>
      </>
    );
  }

  getLoginView() {
    return (
      <Card className={classNames(theme.card, theme.formWrapper)}>
        {this.activeTab ? this.getRegistrationGui() : this.getLoginnGui()}
      </Card>
    );
  }

  getRegistrationView() {
    return (
      <DonePage
        smallHeader={'Thank you for your registration'}
        largeHeader={'Welcome to the Car-Rental'}
        buttonText={'Process to Login'}
        onClick={() => {
          this.registrationView = false;
          this.props.history.push('/login');
        }}
      />
    );
  }

  public render(): JSX.Element {
    if (this.registrationView) {
      return this.getRegistrationView();
    }
    return (
      <div className={theme.pageWrapper}>
        <Card style={{marginTop: "140px"}} className={classNames(theme.card)}>
          {this.activeTab ? null : this.getLoginView()}
        </Card>
        <Snackbar type={this.type} open={this.open} onClose={() => { this.open = false; }}>
          {this.message}
        </Snackbar >
      </div >
    );
  }
}
