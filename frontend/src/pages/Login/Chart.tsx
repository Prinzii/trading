

import * as React from 'react';

import { ChartCanvas, Chart } from "react-stockcharts";
import {
	CandlestickSeries,
	LineSeries,
	BarSeries,
	AlternatingFillAreaSeries,
	StochasticSeries,
	StraightLine,
} from "react-stockcharts/lib/series";
import {
	OHLCTooltip,
	MovingAverageTooltip,
	BollingerBandTooltip,
	StochasticTooltip,
	GroupTooltip,
} from "react-stockcharts/lib/tooltip";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";

import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last } from "react-stockcharts/lib/utils";

import {
	CrossHairCursor,
} from "react-stockcharts/lib/coordinates";

interface ICandleStickStockScaleChartProps {
	data: any[],
	width?: number,
	ratio?: number,
  height: number,
	type: "svg" | "hybrid",
}

class MyCandleStickStockScaleChart extends React.Component<ICandleStickStockScaleChartProps> {
	render() {
		const otherHeight = this.props.height/6;
		const winningHight = this.props.height/6
		const mainHeight = this.props.height-otherHeight - winningHight;
		const { data: initialData, width, ratio } = this.props;

		const xScaleProvider = discontinuousTimeScaleProvider
			.inputDateAccessor(d => d.date);
		const {
			data,
			xScale,
			xAccessor,
			displayXAccessor,
		} = xScaleProvider(initialData);
		const xExtents = [
			xAccessor(last(data)),
			xAccessor(data[data.length - 100])
		];

  	const margin = { left: 70, right: 70, top: 20, bottom: 30 };

  	const gridHeight = this.props.height - margin.top - margin.bottom;
  	const gridWidth = width - margin.left - margin.right;

		const yGrid = { innerTickSize: -1 * gridWidth, tickStrokeOpacity: 0.2 }
		const xGrid = { innerTickSize: -1 * gridHeight, tickStrokeOpacity: 0.2 }

		return (
			<ChartCanvas height={this.props.height}
				ratio={ratio}
				width={width}
				margin={margin}
				type={this.props.type || "svg"}
				seriesName="MSFT"
				data={data}
				xScale={xScale}
				xAccessor={xAccessor}
				displayXAccessor={displayXAccessor}
				xExtents={xExtents}
			>

				<Chart id={1} height={this.props.height-otherHeight - winningHight -margin.bottom-margin.top}  yExtents={d => [d.high, d.low]}>
				//	<XAxis axisAt="bottom" orient="bottom" ticks={6}/>
				//	<YAxis axisAt="left" orient="left" ticks={5} />
        <YAxis axisAt="right" orient="right" ticks={10} {...yGrid} inverted={true}
          tickStroke="#FFFFFF" />
					<LineSeries yAccessor={(d)=>{
						return parseFloat(d.close)}} stroke={"#FF00FF"}/>
				<CandlestickSeries />

				<XAxis axisAt="bottom" orient="bottom"
					showTicks={false}
					outerTickSize={0}
					stroke="#FFFFFF" opacity={1} />

					<CandlestickSeries
						stroke={d => d.close > d.open ? "#6BA583" : "#DB0000"}
						wickStroke={d => d.close > d.open ? "#6BA583" : "#DB0000"}
						fill={d => d.close > d.open ? "#00e05d" : "#DB0000"} />


  				<CrossHairCursor stroke="#FFFFFF" />


					<LineSeries yAccessor={(d)=>{
            return parseFloat(d.predic)}} stroke={"#ff7a04"}/>

				</Chart>
				<Chart id={2}
					yExtents={d => [Math.abs(parseFloat(d.predic) - parseFloat(d.close)), Math.abs(parseFloat(d.predic) - parseFloat(d.close))]}
					height={otherHeight} origin={(w, h) => [0, h-otherHeight-winningHight]} padding={{ top: 10, bottom: 10 }}
				>
					<XAxis axisAt="bottom" orient="bottom"
						showTicks={false}
						outerTickSize={0}
						stroke="#FFFFFF" opacity={0.5} />
					<YAxis axisAt="right" orient="right"
						{...yGrid}
						ticks={5}
						tickStroke="#FFFFFF"
					/>

				<LineSeries yAccessor={(d)=>{
				return Math.abs(parseFloat(d.predic) - parseFloat(d.close))}}  stroke={"#00ff00"}/>
				</Chart>
				<Chart id={3}
					yExtents={d => [(Math.abs(parseFloat(d.predic) - parseFloat(d.close))/d.close)*1.2, (Math.abs(parseFloat(d.predic) - parseFloat(d.close))/d.close)*1.2]}
					height={otherHeight} origin={(w, h) => [0, h-otherHeight-winningHight]} padding={{ top: 10, bottom: 10 }}
				>
				<XAxis axisAt="bottom" orient="bottom"
					showTicks={false}
					outerTickSize={0}
					stroke="#FFFFFF" opacity={0.5} />
					<YAxis axisAt="left" orient="left"
						ticks={5}
						tickStroke="#FFFFFF"
					/>
				<LineSeries yAccessor={(d)=>{
				return Math.abs(parseFloat(d.predic) - parseFloat(d.close))/d.close}}  stroke={"#0000FF"}/>

				</Chart>
				<Chart id={4}
					yExtents={d => [d.myMoney, d.myMoney]}
					height={otherHeight} origin={(w, h) => [0, h-otherHeight]} padding={{ top: 10, bottom: 10 }}
				>
					<XAxis axisAt="bottom" orient="bottom"
						outerTickSize={0}
						{...xGrid}
						tickStroke="#FFFFFF"
						stroke="#FFFFFF" opacity={0.5} />
					<YAxis axisAt="right" orient="right"
						ticks={5}
						tickStroke="#FFFFFF"
					/>

				<AlternatingFillAreaSeries
					baseAt={0}
					yAccessor={(d)=>{
					return d.myMoney}}
				/>
				<StraightLine yValue={0} />

				</Chart>
				<Chart id={5}
					yExtents={d => [d.numberOfShares, d.numberOfShares]}
					height={otherHeight} origin={(w, h) => [0, h-otherHeight]} padding={{ top: 10, bottom: 10 }}
				>
					<XAxis axisAt="bottom" orient="bottom"
						outerTickSize={0}
						{...xGrid}
						tickStroke="#FFFFFF"
						stroke="#FFFFFF" opacity={0.5} />
					<YAxis axisAt="left" orient="left"
						ticks={5}
						tickStroke="#FFFFFF"
					/>


					<LineSeries yAccessor={(d)=>{
						return parseFloat(d.numberOfShares)}} stroke={"#FF00FF"}/>

				</Chart>
			</ChartCanvas>


		);
	}
}


const CandleStickStockScaleChart = fitWidth(MyCandleStickStockScaleChart);

export default CandleStickStockScaleChart;
