import { AddConnectionStore, ConnectionStore } from "@/stores";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Drawer from "@material-ui/core/Drawer";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import WorkIcon from "@material-ui/icons/Work";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import UserIcon from "@material-ui/icons/Person";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { TypeChooser } from "react-stockcharts/lib/helper";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';


import Chart from "./Chart";
import { tsvParse, csvParse } from "d3-dsv";
import { timeParse } from "d3-time-format";

import * as theme from "./theme.scss";

import { observable, toJS } from "mobx";
import { inject, observer } from "mobx-react";

interface ILoginProps {
  history: any;
  location: any;
  connectionStore?: ConnectionStore;
  addConnectionStore?: AddConnectionStore;
}

@inject("connectionStore", "addConnectionStore")
@observer
export default class Login extends React.Component<ILoginProps> {
  @observable showDialog: boolean = false;
  @observable showDrawer: boolean = false;
  @observable loaded: boolean = false;
  @observable port: number = 5000;
  @observable investedMoney: number = 0;

  data: any[];
  parseDate = timeParse("%Y-%m-%d");

  componentDidMount() {
    this.getData().then((data) => {
      this.data = data;
      this.loaded = true;
    });
  }

  parseData(parse) {
    return function (d) {
      d.date = parse(d.date);
      d.open = +d.open;
      d.high = +d.high;
      d.low = +d.low;
      d.close = +d.close;
      d.volume = +d.volume;

      return d;
    };
  }

  shouldBuyOrSell(sockPriceToday, stockPriceTomorrow) {
    const delta = (sockPriceToday - stockPriceTomorrow) / sockPriceToday;
    if (Math.abs(delta) > 0.01) {
      return delta < 0 ? 1 : -1;
    }
    return 0;
  }

  apParseData(data) {
    this.investedMoney = 0;
    const shouldScale = false
    const numberOfShares = 1;
    let scaler = 1
    const json = JSON.parse(data);
    const returnJSON = [];
    const keys = Object.keys(json);
    let winnings = 0;
    let currentSharse = 0;
    for (let i = 1; i < keys.length; i++) {
      const key = keys[i];
      const lastdata = json[keys[i - 1]];
      const data = json[key];

      const sockPriceToday = parseFloat(data["4. close"]);
      const stockPriceTomorrow = parseFloat(data["predic"]);
      if ( shouldScale ) {
        scaler = Math.max(Math.round(Math.abs((sockPriceToday - stockPriceTomorrow)/sockPriceToday)) * 800,1)
      } else {
        scaler = 1
      }
      if (this.shouldBuyOrSell(sockPriceToday, stockPriceTomorrow) === 1) {
        // Buy some shares
        currentSharse += numberOfShares*scaler;
        winnings -= sockPriceToday * numberOfShares*scaler;
        this.investedMoney += sockPriceToday * numberOfShares*scaler;
      } else if (
        this.shouldBuyOrSell(sockPriceToday, stockPriceTomorrow) === -1
      ) {
        // Sell some shares
        if (currentSharse-numberOfShares*scaler >= 0) {
          currentSharse -= numberOfShares*scaler;
          winnings += sockPriceToday * numberOfShares*scaler;
        }
      }

      returnJSON.push({
        date: new Date(key),
        close: data["4. close"],
        adjustedClose: data["5. adjusted close"],
        open: data["1. open"],
        high: data["2. high"],
        low: data["3. low"],
        predic: lastdata["predic"],
        volume: data["6. volume"],
        myMoney: currentSharse * data["4. close"] + winnings,
        numberOfShares: currentSharse,
      });
    }
    return returnJSON;
  }

  getData() {
    const promiseMSFT = fetch(`http://localhost:${this.port}/companies`)
      .then((response) => response.text())
      .then((data) => {
        //const tmp = tsvParse(data, this.parseData(this.parseDate))
        return this.apParseData(data);
      });
    return promiseMSFT;
  }

  getChart() {
    if (!this.loaded) {
      return <div>Loading...</div>;
    }
    return <Chart type={"svg"} height={700} data={this.data} />;
  }


  public render(): JSX.Element {

    return (
      <div className={theme.pageWrapper}>
        <AppBar style={{display: "none"}}position="static">
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={() => {
                this.showDrawer = true;
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              Data-Analysis
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          style={{ minWidth: "240px" }}
          open={this.showDrawer}
          onBackdropClick={() => {
            this.showDrawer = false;
          }}
        >
          <List>
            <ListItem button>
              <ListItemIcon>
                <WorkIcon />
              </ListItemIcon>
              <ListItemText primary={"Dashboard"} />
            </ListItem>
            <ListItem
              button
              onClick={() => {
                window.location.href = "user";
              }}
            >
              <ListItemIcon>
                <UserIcon />
              </ListItemIcon>
              <ListItemText primary={"User"} />
            </ListItem>
          </List>
        </Drawer>
        <FormControl style={{margin: "15px"}}>
          <InputLabel id="demo-simple-select-outlined-label">Company</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={this.port}
            className={theme.companyWrapper}
            onChange={(event)=>{
              this.port = parseInt(event.target.value as string)
              this.loaded = false;
              this.getData().then((data) => {
                this.data = data;
                this.loaded = true;
              });
            }}
          >
            <MenuItem value={5000}>The Coca-Cola Company</MenuItem>
            <MenuItem value={5050}>Global Cord Blood Corp</MenuItem>
            <MenuItem value={7000}>Microsoft</MenuItem>
            <MenuItem value={7077}>Apple</MenuItem>
          </Select>
        </FormControl>
        <div style={{position: "fixed",top: "40px",right: "15px",color: "gray",fontSize: "18px", textAlign: "right"}}>
          <div>
            <span>Investment: </span>
            <span>{this.investedMoney.toFixed(2)}$</span>
          </div>
          <div>
            <span>Winning: </span>
            <span>{((this.loaded? this.data[this.data.length-1].myMoney/this.investedMoney: 0)*100).toFixed(2)}%</span>
          </div>
        </div>
        {this.getChart()}
      </div>
    );
  }
}
